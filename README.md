﻿# PHP-SCRIPTS

## Getting started

Welcome to PHP-Scripts! This repository contains PHP-CLI scripts designed to demonstrate the ease and usefulness of PHP as a quick scripting language. Please note that these scripts are more like a Bash Script than they are for displaying on the internet, and are for personal use.

To get started, please ensure that php-cli is installed on your system by following the instructions in the [How to Install] section below.(##how-to-install)

## How to install

If you are on a Debian Linux OS, you can install php-cli by running the following command in your terminal:
```
sudo apt install php-cli
```
Please be aware that installing php-cli is not recommended for beginners and can have an impact on your system. If you're new to using php-cli, it's recommended that you install it in a virtual box for the first time.

## Description

This repository contains PHP-CLI scripts that can be run via the command line. These scripts are designed to be quick and easy to implement into other programs.


## Installation

These scripts are developed and tested on Debian-based OS, such as Ubuntu, Mint, and MX Linux. Some of the scripts may require other programs to run properly, so please check each file's requirements at the beginning of the file.

## Usage

The use case is to use the command line over other tools, as it can be quicker and easier to implement into other programs. These scripts are built small to make it easy to change and can be used to create larger programs or as standalone scripts.

## Support

If you have any suggestions or feedback, feel free to contact us on our Facebook page at https://www.facebook.com/groups/648648643592658. If you would like to contribute and support us, please check out our Patreon site at https://www.patreon.com/user?u=74944932.

## Roadmap

Most of the files in this repository are stable and do not require any changes. However, these scripts are built small and can be easily modified to fit your needs.

## Authors and acknowledgment

This repository is maintained by Asher Simcha. Each file may have its own acknowledgment as well.

## License

Unless otherwise specified the following scripts are:

Copyright 2023, Asher Simcha

Copying and distribution of this file, with or without modification, are permitted in any medium without royalty, provided the copyright notice and this notice are preserved. These files is offered as-is, without any warranty.

### Warning

Please be aware that installing php-cli is not recommended for beginners and can have an impact on your system. If you're new to using php-cli, it's recommended that you install it in a virtual box for the first time.

### how-to-install-php-cli

sudo apt install php-cli
